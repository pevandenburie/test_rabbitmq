#! /usr/bin/env node

var amqp = require('amqplib/callback_api');

amqp.connect('amqp://rabbitmq', function(err, conn) {
  if (err) {
    console.log("AMQP connection error: "+err);
  }
  else {
    conn.createChannel( function(err, ch) {
      var q = 'myQueue';

      ch.assertQueue(q, {durable:false});

      console.log(" [x] Waiting for messages in %s. Press CTRL+C to exit.", q);

      ch.consume(q, function(msg) {

        var secs = msg.content.toString().split('.').length - 1;

        console.log(" [x] Received: "+ msg.content.toString());

        setTimeout( function() {
          ch.ack(msg);
          console.log(" [x] Done (%s secs).", secs);
        }, secs * 1000);
      }, {noAck: false});
    });
  }
});
