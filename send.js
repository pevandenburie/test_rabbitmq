#!/usr/bin/env node

var amqp = require('amqplib/callback_api');

amqp.connect('amqp://rabbitmq', function(err, conn) {
  if (err) {
    console.log("AMQP error: "+err);
  } else {
    console.log("AMQP connected");

    conn.createChannel( function(err, ch){
      if (err) {
        console.log("Channel error: "+ err);
      }
      else {
        var q = "myQueue";
        var data = "Hello World";

        ch.assertQueue(q, {durable: false});
        // Note: on Node 6 Buffer.from(msg) should be used
        ch.sendToQueue(q, new Buffer(data));
        console.log(" [x] sent to queue: " + data);

        setTimeout( function() { conn.close(); process.exit(0); }, 500 );
      }
    } );
  }
});
