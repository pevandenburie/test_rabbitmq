FROM node:argon

# Install app dependencies
RUN npm install -g amqplib

# Bundle app source
RUN mkdir -p /usr/src/app
COPY . /usr/src/app

WORKDIR /usr/src/app

CMD ["tail", "-f", "/dev/null"]
