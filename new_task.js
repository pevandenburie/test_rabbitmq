#!/usr/bin/env node

var amqp = require('amqplib/callback_api');

var msg = process.argv.slice(2).join(' ') || "Hello World!";


amqp.connect('amqp://rabbitmq', function(err, conn) {
  if (err) {
    console.log("AMQP error: "+err);
  } else {
    console.log("AMQP connected");

    conn.createChannel( function(err, ch){
      if (err) {
        console.log("Channel error: "+ err);
      }
      else {
        var q = "myQueue";

        ch.assertQueue(q, {durable: false});
        // Note: on Node 6 Buffer.from(msg) should be used
        ch.sendToQueue(q, new Buffer(msg), {persistent: true});
        console.log(" [x] sent to queue: " +msg);

        setTimeout( function() { conn.close(); process.exit(0); }, 500 );
      }
    } );
  }
});
