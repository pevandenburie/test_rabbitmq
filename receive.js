#! /usr/bin/env node

var amqp = require('amqplib/callback_api');

amqp.connect('amqp://rabbitmq', function(err, conn) {
  conn.createChannel( function(err, ch) {
    var q = 'myQueue';

    ch.assertQueue(q, {durable:false});

    console.log(" [x] Waiting for messages in %s. Press CTRL+C to exit.", q);

    ch.consume(q, function(msg) {
      console.log(" [x] Received: "+ msg.content.toString());
    }, {noAck: true});

  });
});
