Test of the RabbitMQ tutorial using Docker
==========================================

This is an adaptation of the [official RabbitMQ tutorial](http://www.rabbitmq.com/tutorials/tutorial-one-javascript.html), with the use of Docker containers.
The point is to avoid installing RabbitMQ and AMQP libs on your machine, and also learn how to use containers in a basic usage! ;-)


## Sandbox of the test application using Docker

Move into the repo folder. If you have a look at the JS files, they match the RabbitMQ tutorial. With one adaptation: I changed the "localhost" AMQP connection address with "rabbitmq", as Docker will do the name resolution:

    amqp.connect('amqp://rabbitmq', function(err, conn) {      // Instead of 'amqp://localhost'
      ...


Build docker images. The [official RabbitMQ image](https://hub.docker.com/_/rabbitmq/) will be downloaded if not available.

    $ docker-compose build

Run the images. This will start two containers: one for RabbitMQ (```testrabbitmq_rabbitmq_1```) and one for the test application (```testrabbitmq_my-test-rabbit```).

    $ docker-compose up -d

    $ docker ps
    CONTAINER ID        IMAGE                         COMMAND                  CREATED             STATUS              PORTS                                NAMES
    aee98408be37        testrabbitmq_my-test-rabbit   "tail -f /dev/null"      5 seconds ago       Up 4 seconds                                             testrabbitmq_my-test-rabbit_1
    f3d115bf4885        rabbitmq:latest               "docker-entrypoint.sh"   10 minutes ago      Up 5 seconds        4369/tcp, 5671-5672/tcp, 25672/tcp   testrabbitmq_rabbitmq_1


## Run the test application from its container

Connect to the test application container:

    & docker exec -it testrabbitmq_my-test-rabbit_1 /bin/bash

Launch the test:

    root@my-test-rabbit_1:/usr/src/app# node send.js
    AMQP connected
     [x] sent to queue: Hello World
    root@my-test-rabbit_1:/usr/src/app# node send.js
    ...
    root@my-test-rabbit_1:/usr/src/app# node send.js
    ...


When launching a bash in RabbitMQ container, we can check the queue contains 3 messages:

    $ docker exec -it testrabbitmq_rabbitmq_1 /bin/bash

    root@rabbitmq_1:/# rabbitmqctl list_queues
    Listing queues ...
    myQueue	3
    ...done.


Consume the messages:

    root@my-test-rabbit_1:/usr/src/app# node receive.js
     [x] Waiting for messages in myQueue. Press CTRL+C to exit.
     [x] Received: Hello World
     [x] Received: Hello World
     [x] Received: Hello World


## Tutorial part 2

Open a shell on test app container and run the worker:

    $ docker exec -it testrabbitmq_my-test-rabbit_1 /bin/bash         # twice

    root@my-test-rabbit_1:/usr/src/app# node worker.js    # shell 1
     [x] Waiting for messages in myQueue. Press CTRL+C to exit.


From another shell, create new tasks on this same container by using ```docker exec``` command:

    $ docker exec -it testrabbitmq_my-test-rabbit_1 node new_task.js hello....................

    $ docker exec -it testrabbitmq_my-test-rabbit_1 node new_task.js bye.....


The worker will receive the tasks:

    root@my-test-rabbit_1:/usr/src/app# node worker.js
     [x] Waiting for messages in myQueue. Press CTRL+C to exit.

     [x] Received: hello....................
     [x] Received: bye.....
     [x] Done (5000).
     [x] Done (20000).


As worker only send the ACK once task is processed, the queue will show the messages still in the queue until "Done" is printed:

    root@rabbitmq_1:/# rabbitmqctl list_queues
    Listing queues ...
    myQueue	2


## Conclusion

That's all! By using Docker even for a simple application such as this RabbitMQ tutorial, we can see how simple it is, and the benefit of just using pre-built images from [Docker hub](https://hub.docker.com), avoiding installation of any library on your machine.
